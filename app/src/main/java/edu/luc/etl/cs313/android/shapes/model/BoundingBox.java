package edu.luc.etl.cs313.android.shapes.model;
import android.graphics.Rect;

import java.util.List;
import java.util.Arrays;

import static java.util.Collections.max;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle)

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
        /**
        final Rectangle r = (Rectangle) f.getShape();
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0,0,new Rectangle(width,height));
         */
        return f.getShape().accept(this);
    }

	@Override
	public Location onGroup(final Group g) {
        /**
         final List<? extends Shape> shapes = g.getShapes();
         int [] x_axis =  new int[shapes.size()];
         int [] x_width =  new int[shapes.size()];
         int [] y_axis = new int[shapes.size()];
         int [] y_height = new int[shapes.size()];
         int cnt = 0 ;
         for (Shape shape : shapes) {
             if(shape.accept(this)!=null){
                 Location box = shape.accept(this);
                 Rectangle r = (Rectangle) box.getShape();
                 x_axis[cnt] += box.getX();
                 y_axis[cnt] += box.getY();
                 x_width[cnt] += r.getWidth();
                 y_height[cnt] += r.getHeight();
             }else{
                x_axis[cnt] += onGroup((Group) shape).getX();
                y_axis[cnt]+= onGroup((Group) shape).getX();
             }
            cnt++;
         }
         Arrays.sort(x_axis);
         Arrays.sort(y_axis);
         Integer width = x_width[x_width.length-1]-x_axis[0];
         Integer height = y_height[y_height.length-1]-y_axis[0];

         return new Location(x_axis[0],y_axis[0], new Rectangle(width,height));
        **/
        Integer minX = null;
        Integer minY = null;
        Integer maxX = null;
        Integer maxY = null;
        for (Shape shape : g.getShapes()) {
            Location boundingBox = shape.accept(this);
            Rectangle rectangle = (Rectangle) boundingBox.getShape();
            if (minX == null && maxX == null) {
                minX = boundingBox.getX();
                maxX = boundingBox.getX() + rectangle.getWidth();
            } else if (boundingBox.getX() < minX) {
                minX = boundingBox.getX();
            }
            if (maxX < boundingBox.getX() + rectangle.getWidth()) {
                maxX = boundingBox.getX() + rectangle.getWidth();
            }
            if (minY == null && maxY == null) {
                minY = boundingBox.getY();
                maxY = boundingBox.getY() + rectangle.getHeight();
            } else if (boundingBox.getY() < minY) {
                minY = boundingBox.getY();
            }
            if (maxY < boundingBox.getY() + rectangle.getHeight()) {
                maxY = boundingBox.getY() + rectangle.getHeight();
            }
        }

        return new Location(minX, minY, new Rectangle(maxX-minX, maxY-minY));

    }

	@Override
	public Location onLocation(final Location l) {
        /**
		final int x = l.getX();
		final int y = l.getY();
		final Rectangle r = (Rectangle) l.getShape();
		return new Location(x,y, new Rectangle(r.getWidth(),r.getHeight()));
         **/
        Location boundingBox = l.getShape().accept(this);
        return new Location(l.getX()+boundingBox.getX(), l.getY()+boundingBox.getY(), boundingBox.getShape());
    }

	@Override
	public Location onRectangle(final Rectangle r) {
        return new Location(0, 0, r);
    }

	@Override
	public Location onStroke(final Stroke c) {
        /**
        final Rectangle r = (Rectangle) c.getShape();
        final int width = r.getWidth();
        final int height = r.getHeight();
        return new Location(0,0,new Rectangle(width,height));
         **/
        return c.getShape().accept(this);
    }

	@Override
	public Location onOutline(final Outline o) {
        /**
		final Rectangle r = (Rectangle) o.getShape();
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0,0,new Rectangle(width,height));
         **/
        return o.getShape().accept(this);
    }

	@Override
	public Location onPolygon(final Polygon s) {
        return onGroup(s);
        /**
		final List<? extends Point> pointList = s.getPoints();
        int [] x_axis =  new int[pointList.size()];
        int [] y_axis = new int[pointList.size()];
        int cnt = 0 ;
        for (Point p : pointList) {
            x_axis[cnt]= p.getX();
            y_axis[cnt]= p.getY();
            cnt++;
        }
        Arrays.sort(x_axis);
        Arrays.sort(y_axis);
        Integer width = x_axis[x_axis.length-1]-x_axis[0];
		Integer height = y_axis[x_axis.length-1]-y_axis[0];
		return new Location(x_axis[0],y_axis[0],new Rectangle(width,height));
         **/
	}

}