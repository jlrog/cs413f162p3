package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import edu.luc.etl.cs313.android.shapes.model.*;
import java.util.ArrayList;
import java.util.List;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas;
		this.paint = paint;
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStroke(final Stroke c) {
		paint.setStyle(Style.STROKE);
		paint.setColor(c.getColor());
		c.getShape().accept(this);
		paint.setStyle(Style.STROKE);
		paint.setColor(Color.WHITE);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for(Shape shape:g.getShapes()){
			if(shape instanceof Group){
				onGroup((Group) shape);
			}else{
				shape.accept(this);
			}
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(),l.getY());
		l.getShape().accept(this);
		canvas.translate(-l.getX(),-l.getY());
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		float width = (float) r.getWidth();
		float height = (float) r.getHeight();
		canvas.drawRect(0,0,width,height, paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		return null;
	}

	@Override
	public Void onPolygon(final Polygon s) {

		int cnt = 0;
		List<Float> list = new ArrayList<Float>();

		for (Point p:s.getPoints()){
			list.add((float) p.getX());
			list.add((float) p.getY());
			if (list.size()%4==0){
				list.add(cnt+2,list.get(cnt));
				list.add(cnt+3,list.get(cnt+1));
				cnt = cnt+2;
			}
			cnt=cnt+2;
		}

		list.add(list.get(0));
		list.add(list.get(1));

		float[] pts = new float[list.size()];
		for(int i = 0; i < list.size(); i++) pts[i] = list.get(i);

		canvas.drawLines(pts, paint);
		return null;
		/*
				int cnt = 0;
				for (Point p:s.getPoints()) {
					pts[cnt]=p.getX();
					pts[cnt+1]=p.getY();
					cnt = cnt + 2;
				}

		        for (int i = 0; i < pts.length; i++) {
		            if (i > pts.length - 4) {
		                break;
		            }
		            float firstX = pts[i];
		            float firstY = pts[i + 1];
		            float secondX = pts[i + 2];
		            float secondY = pts[i + 3];
		            i++;
		            canvas.drawLine(firstX, firstY, secondX, secondY, paint);
		        }

		        canvas.drawLine(pts[0], pts[1], pts[pts.length-2], pts[pts.length-1], paint);
						return null;
		*/

	}
}
