[ ![Codeship Status for jrodriguezorjuela/cs413f162p3](https://app.codeship.com/projects/bd200de0-783e-0134-e7ac-3a70d1fe3dd2/status?branch=master)](https://app.codeship.com/projects/180160)

# Android Emulator output

|Nexus 5X API24|Shapes APP|
---------------|----------|
|![](emulator1.png)|![](emulator2.png)|
|![](emulator3.png)|

# Passed Tests

## 22 tests completed, 22 passed

## **TODO** - TestDraw.java

CODE LINE | TASK | COMMENT
------|------|------
java:55|testSimpleGroup|PASSED
java:66|testComplexGroup|PASSED
java:47|testSimpleLocation|PASSED

## **TODO** - TestBoundingBox.java
CODE LINE | TASK | COMMENT
------|------|------
java:97|testOutline|PASSED
java:107|testPolygon|PASSED
java:77|testGroupSimple|PASSED
java:57|testFilled|PASSED
java:87|testGroupComplex|PASSED
java:67|testStroke|PASSED
java:37|testRectangle|PASSED

## **TODO** - TestSize.java
CODE LINE | TASK | COMMENT
------|------|------
java:37|testLocation|PASSED
java:67|testOutline|PASSED
java:42|testFill|PASSED
java:72|testPolygon|PASSED
java:57|testGroupMiddle|PASSED
java:52|testGroupSimple|PASSED
java:27|testCircle|PASSED
java:62|testGroupComplex|PASSED
java:47|testStroke|PASSED
java:32|testRectangle|PASSED

# Learning Objectives

* Familiarity with a simple graphical (output-only) Android project
* Basic design patterns and their purpose
    * Composite pattern
    * Decorator pattern
    * Visitor pattern

# Setting up the Environment

Check out the project using Android Studio. This creates the `local.properties` file
with the required line

    sdk.dir=< PATH_TO_ANDROID_STUDIO>

# Running the Application

In Android Studio: `Run > Run app`

# Running the Tests

## Unit tests

In Android Studio:

* `View > Tool Windows > Build Variants`
* `Test Artifact: Unit Tests`
* right-click on `app/java/edu...shapes (test)`, then choose `Run Tests in edu...`

You can also use Gradle:

    $ ./gradlew testDebug

You can view the resulting test reports in HTML by opening this file in your browser:

    app/build/reports/tests/debug/index.html
